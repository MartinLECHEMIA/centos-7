# Post-installation setup script for CentOS 7 servers 

(c) Niki Kovacs, 2020

Ce repertoire met à disposition un script de configuration "magic" en post-installation
pour les serveurs tournant sur CentOS 7 ainsi que quelques autres scripts utiles
et des templates de fichier de configuration pour les services les plus communs

## En résumé

Suivre les étapes suivantes.

  1. Installer une version minimale de CentOS 7

  2. Créer un compte non-`root` avec les droits d'administrateurs

  3. Installer Git: `sudo yum install git`

  4. Récupérer les scripts: `git clone https://gitlab.com/kikinovak/centos-7.git`

  5. Changer de répertoire: `cd centos-7`

  6. Lancer le script: `sudo ./centos-setup.sh --setup`

  7. Allez vous chercher un café, ça va prendre du temps!

  8. Redémarrez.


## Personnalisation du serveur CentOS

Rendre une installation minimale de centOS fonctionnel requiert souvent
l'utilisation de plusieurs opérations chronophages. Vous expérience peut être
différent mais souvent, nous devons faire ces étapes sur une installation fraiche de CentOS

  * On personnalise le shell avec des prompts, alias

  * On personnalise l'éditeur vim

  * On met en place les repositories officiels et tiers

  * On installe un set complet de commande

  * On supprime les packages inutiles

  * On autorise es admins à accéder aux logs

  * On désactive l'IPV6 et on reconfigure les services en conséquences

  * On configue un mot de passe pour `sudo`

  * Etc.

Le script `centos-setup.sh` fait toutes ces opérations.

Pour configurer Bash et Vim et rendre plus lisible la résolution de la console par défaut:

```
# ./centos-setup.sh --shell
```

Pour configurer les repositories officiels et tier:

```
# ./centos-setup.sh --repos
```

Pour installer les groupes de packages `Core` et `Base` avec quelques autres outils

```
# ./centos-setup.sh --extra
```

Pour supprimer les packages inutiles:

```
# ./centos-setup.sh --prune
```

Pour autoriser les admins à accéder aux logs systèmes:

```
# ./centos-setup.sh --logs
```

Désactiver IPv6 et reconfigurer les services de base en conséquences:

```
# ./centos-setup.sh --ipv4
```

On configure un mot de passe persistant pour sudo:

```
# ./centos-setup.sh --sudo
```

Pour activer toutes les commandes citées au dessus d'un seul coup:

```
# ./centos-setup.sh --setup
```

Supprimer les packages et récupérer un système propre:

```
# ./centos-setup.sh --strip
```

Afficher le message d'aide:

```
# ./centos-setup.sh --help
```

Si vous voulez savoir exactement ce qu'il se passe, ouvrez un second terminal
et pour voir les logs:

```
$ tail -f /tmp/centos-setup.log
```

